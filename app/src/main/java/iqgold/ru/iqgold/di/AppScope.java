package iqgold.ru.iqgold.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Admin on 28.11.2017.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface AppScope {
}
