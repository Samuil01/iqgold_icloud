package iqgold.ru.iqgold.di.components;

import android.content.Context;

import dagger.BindsInstance;
import dagger.Component;
import iqgold.ru.iqgold.di.AppScope;
import iqgold.ru.iqgold.di.models.AppModule;
import iqgold.ru.iqgold.di.models.NavigationModule;
import iqgold.ru.iqgold.ui.activity.checkout.CheckoutPresenter;
import iqgold.ru.iqgold.ui.activity.contacts.ContactsPresenter;
import iqgold.ru.iqgold.ui.activity.oneproduct.OneProductPresenter;
import iqgold.ru.iqgold.ui.activity.product.PresenterProducts;
import iqgold.ru.iqgold.ui.categories.PresenterCategory;
import iqgold.ru.iqgold.ui.main.MainActivity;

/**
 * Created by Admin on 28.11.2017.
 */

@Component(modules = {AppModule.class, NavigationModule.class})
@AppScope
public interface AppComponent {

    void inject(MainActivity mainActivity);
    void inject(PresenterCategory presenterCategory);
    void inject(PresenterProducts presenterProducts);
    void inject(OneProductPresenter oneProductPresenter);
    void inject(ContactsPresenter contactsPresenter);

    void inject(CheckoutPresenter checkoutPresenter);

    @Component.Builder
    interface Builder {
        AppComponent build();
        @BindsInstance Builder appContext(Context context);
    }

}
