package iqgold.ru.iqgold.di.models;

import dagger.Module;
import dagger.Provides;
import iqgold.ru.iqgold.di.AppScope;
import ru.terrakok.cicerone.Cicerone;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;

/**
 * Created by Admin on 28.11.2017.
 */

@Module
public class NavigationModule {

    private Cicerone<Router> cicerone;

    public NavigationModule() {
        cicerone = Cicerone.create();
    }

    @Provides
    @AppScope
    Router provideRouter() {
        return cicerone.getRouter();
    }

    @Provides
    @AppScope
    NavigatorHolder provideNavigatorHolder() {
        return cicerone.getNavigatorHolder();
    }

}
