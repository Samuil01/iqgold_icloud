package iqgold.ru.iqgold.di.models;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import iqgold.ru.iqgold.data.model.Preferences;
import iqgold.ru.iqgold.data.network.ApiInterface;
import iqgold.ru.iqgold.data.network.INetModel;
import iqgold.ru.iqgold.data.network.NetModel;
import iqgold.ru.iqgold.data.network.ServiceFactory;
import iqgold.ru.iqgold.di.AppScope;

/**
 * Created by Admin on 28.11.2017.
 */

@Module
public class AppModule {

    @AppScope
    @Provides
    INetModel provideNetModel(ApiInterface apiInterface) {
        return new NetModel(apiInterface);
    }


    @AppScope
    @Provides
    public ApiInterface provideApiInterface() {
        return ServiceFactory.getService();
    }

    @AppScope
    @Provides
    public Preferences providePreferences(Context context) {
        return new Preferences(context);
    }

}
