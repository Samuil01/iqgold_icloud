package iqgold.ru.iqgold.other;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Admin on 04.12.2017.
 */

public class RxUtils {

    public void ObservableCreate() {

        Observable.zip(Observable.just(1, 2, 3), Observable.just(4, 5, 6),
                new BiFunction<Integer, Integer, Integer>() {
                    @Override
                    public Integer apply(Integer val0, Integer val1) throws Exception {
                        return val0 + val1;
                    }
                }).subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Integer>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Integer integer) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

}
