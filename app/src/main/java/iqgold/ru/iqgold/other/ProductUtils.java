package iqgold.ru.iqgold.other;

import java.util.ArrayList;
import java.util.List;

import iqgold.ru.iqgold.data.model.dto.Product;

/**
 * Created by s.kurbanov on 09.12.17.
 */

public class ProductUtils {
    public static List<Product> searchProduct(List<Product> products, String s) {
        if (s.length() < 1) {
            return products;
        }
        List<Product> result = new ArrayList();
        for (Product product : products) {
            if (product.getTitle().toLowerCase().contains(s.toLowerCase())) {
                result.add(product);
            }
        }
        return result;
    }
}
