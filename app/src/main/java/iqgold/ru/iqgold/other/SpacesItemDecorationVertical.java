package iqgold.ru.iqgold.other;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Admin on 17.10.2017.
 */

public class SpacesItemDecorationVertical extends RecyclerView.ItemDecoration {

    private int space;

    public SpacesItemDecorationVertical(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
        /*outRect.left = space;
        outRect.right = space;
        outRect.bottom = space;*/

        outRect.top = space;

        /*if (parent.getChildLayoutPosition(view) == 0) {
            outRect.top = space / 2;
            outRect.left = space;
            outRect.right = space / 2;
            outRect.bottom = space / 2;
        } else {
            outRect.top = space / 2;
            outRect.left = space / 2;
            outRect.right = space;
            outRect.bottom = space / 2;
        }*/
    }
}
