package iqgold.ru.iqgold;

import android.app.Application;

import com.facebook.stetho.Stetho;

import iqgold.ru.iqgold.di.components.AppComponent;
import iqgold.ru.iqgold.di.components.DaggerAppComponent;

/**
 * Created by Admin on 28.11.2017.
 */

public class App extends Application {

    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = buildComponent();
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this);
        }
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    private AppComponent buildComponent() {
        return DaggerAppComponent.builder()
                .appContext(getApplicationContext())
                // Контекст будет нужен когда буду использовать ORM greenDao
                /*.context(this)*/
                .build();
    }
}
