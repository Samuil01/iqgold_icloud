package iqgold.ru.iqgold.data.model.dto;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 28.11.2017.
 */

public class Attribute {

    @SerializedName("key")
    String key;

    @SerializedName("value")
    String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
