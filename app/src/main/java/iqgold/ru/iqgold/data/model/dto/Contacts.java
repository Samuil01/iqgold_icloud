package iqgold.ru.iqgold.data.model.dto;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 28.11.2017.
 */

public class Contacts {

    long id = 0;

    @SerializedName("phoneNumber")
    String phoneNumber;

    @SerializedName("skypeNumber")
    String skypeNumber;

    @SerializedName("email")
    String email;

    @SerializedName("address")
    String address;

    @SerializedName("schedule")
    String schedule;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getSkypeNumber() {
        return skypeNumber;
    }

    public void setSkypeNumber(String skypeNumber) {
        this.skypeNumber = skypeNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }
}
