package iqgold.ru.iqgold.data.model.dto;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 28.11.2017.
 */

public class Order {

    @SerializedName("product")
    long product;

    @SerializedName("name")
    String name;

    @SerializedName("email")
    String email;

    @SerializedName("phoneNumber")
    String phoneNumber;

    public long getProduct() {
        return product;
    }

    public void setProduct(long product) {
        this.product = product;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
