package iqgold.ru.iqgold.data.network;

import java.util.List;
import java.util.Map;

import iqgold.ru.iqgold.data.model.dto.Category;
import io.reactivex.Observable;
import iqgold.ru.iqgold.data.model.dto.Contacts;
import iqgold.ru.iqgold.data.model.dto.MessageOrder;
import iqgold.ru.iqgold.data.model.dto.Order;
import iqgold.ru.iqgold.data.model.dto.Product;
import iqgold.ru.iqgold.data.model.dto.Sorting;

public interface INetModel {

    Observable<List<Category>> getCategories(Long id);

    Observable<List<Sorting>> getSorting();

    Observable<Product> getOneProduct(Long id);

    Observable<Contacts> getContacts();

    Observable<MessageOrder> sendOrder(Map<String, String> order);
}