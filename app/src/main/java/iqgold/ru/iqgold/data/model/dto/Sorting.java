package iqgold.ru.iqgold.data.model.dto;


/**
 * Created by Admin on 28.11.2017.
 */

public class Sorting {

    String title;
    String code;
    String order;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return title;
    }
}
