package iqgold.ru.iqgold.data.model.dto;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 28.11.2017.
 */

public class Category {

    long id;

    @SerializedName("title")
    String title;

    @SerializedName("contentType")
    String contentType;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}
