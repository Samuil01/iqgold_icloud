package iqgold.ru.iqgold.data.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

import iqgold.ru.iqgold.data.model.dto.Contacts;
import okhttp3.Cookie;

public class Preferences {

    private static final String KEY_CONTACTS = "KEY_CONTACTS";

    private SharedPreferences preferences;
    private SharedPreferences.Editor prefEditor;
    private Gson gson = new Gson();
    private Cookie cookie;

    public Preferences(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        prefEditor = preferences.edit();
    }

    public Contacts getContacts() {
        return gson.fromJson(getString(KEY_CONTACTS), Contacts.class);
    }

    public void setUser(Contacts user) {
        putString(KEY_CONTACTS, gson.toJson(user));
    }

    private void putString(String key, String s) {
        prefEditor.putString(key, s)
                .commit();
    }

    public void eraseUser() {
        putString(KEY_CONTACTS, "");
    }

    public String getString(String key) {
        return preferences.getString(key, "");
    }

}
