package iqgold.ru.iqgold.data.model.dto;


/**
 * Created by Admin on 28.11.2017.
 */

public class RealmString {

    public RealmString() {
    }

    public RealmString(String name) {
        this.name = name;
    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
