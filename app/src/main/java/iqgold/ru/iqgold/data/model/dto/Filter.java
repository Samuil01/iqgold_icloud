package iqgold.ru.iqgold.data.model.dto;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 28.11.2017.
 */

public class Filter {

    @SerializedName("type")
    String type;

    @SerializedName("title")
    String title;

    @SerializedName("minParameter")
    String minParameter;

    @SerializedName("maxParameter")
    String maxParameter;

    @SerializedName("minValue")
    double minValue;

    @SerializedName("maxValue")
    double maxValue;

    @SerializedName("parameter")
    String parameter;

    @SerializedName("values")
    List<FilterValues> values;

    Map map; //  = new TreeMap<String, Filter>()

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }


    public String getMinParameter() {
        return minParameter;
    }

    public void setMinParameter(String minParameter) {
        this.minParameter = minParameter;
    }

    public String getMaxParameter() {
        return maxParameter;
    }

    public void setMaxParameter(String maxParameter) {
        this.maxParameter = maxParameter;
    }

    public double getMinValue() {
        return minValue;
    }

    public void setMinValue(double minValue) {
        this.minValue = minValue;
    }

    public double getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(double maxValue) {
        this.maxValue = maxValue;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    public List<FilterValues> getValues() {
        return values;
    }

    public void setValues(List<FilterValues> values) {
        this.values = values;
    }
}
