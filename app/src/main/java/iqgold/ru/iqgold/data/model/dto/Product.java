package iqgold.ru.iqgold.data.model.dto;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Admin on 28.11.2017.
 */

public class Product {

    long id;

    @SerializedName("title")
    String title;

    @SerializedName("description")
    String description;

    @SerializedName("mainImage")
    String mainImage;

    @SerializedName("gallery")
    List<String> gallery;

    @SerializedName("price")
    int price;

    @SerializedName("oldPrice")
    int oldPrice;

    @SerializedName("mainAttributes")
    List<Attribute> mainAttributes;

    @SerializedName("attributes")
    List<Attribute> attributes;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMainImage() {
        return mainImage;
    }

    public void setMainImage(String mainImage) {
        this.mainImage = mainImage;
    }

    public List<String> getGallery() {
        return gallery;
    }

    public void setGallery(List<String> gallery) {
        this.gallery = gallery;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(int oldPrice) {
        this.oldPrice = oldPrice;
    }

    public List<Attribute> getMainAttributes() {
        return mainAttributes;
    }

    public void setMainAttributes(List<Attribute> mainAttributes) {
        this.mainAttributes = mainAttributes;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }
}