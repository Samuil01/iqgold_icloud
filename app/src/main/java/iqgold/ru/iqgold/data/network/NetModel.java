package iqgold.ru.iqgold.data.network;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.ObservableTransformer;
import iqgold.ru.iqgold.data.model.dto.Category;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import iqgold.ru.iqgold.data.model.dto.Contacts;
import iqgold.ru.iqgold.data.model.dto.Filter;
import iqgold.ru.iqgold.data.model.dto.MessageOrder;
import iqgold.ru.iqgold.data.model.dto.Order;
import iqgold.ru.iqgold.data.model.dto.Product;
import iqgold.ru.iqgold.data.model.dto.Sorting;

public class NetModel implements INetModel {

    ApiInterface apiServer;

    @Inject
    public NetModel(ApiInterface apiServer) {
        this.apiServer = apiServer;
    }

    private <T> ObservableTransformer<T, T> applySchedulers() {
        return tObservable -> tObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<List<Category>> getCategories(Long id) {
        return apiServer.getCategories(id)
                .compose(applySchedulers());
    }

    @Override
    public Observable<List<Sorting>> getSorting() {
        return apiServer.getSorting()
                .compose(applySchedulers());
    }

    @Override
    public Observable<Product> getOneProduct(Long id) {
        return apiServer.getOneProduct(id)
                .compose(applySchedulers());
    }

    @Override
    public Observable<Contacts> getContacts() {
        return apiServer.getContacts()
                .compose(applySchedulers());
    }

    @Override
    public Observable<MessageOrder> sendOrder(Map<String, String> params) {
        return apiServer.sendOrder(params)
                .compose(applySchedulers());
    }


    public Observable<List<Product>> getProducts(Map<String, String> options) {
        return apiServer.getProducts(options)
                .compose(applySchedulers());
    }

    public Observable<List<Filter>> getFilters(Map<String, String> options) {
        return apiServer.getFilters(options)
                .compose(applySchedulers());
    }
}