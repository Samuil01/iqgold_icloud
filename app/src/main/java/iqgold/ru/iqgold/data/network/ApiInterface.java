package iqgold.ru.iqgold.data.network;

import java.util.List;
import java.util.Map;

import iqgold.ru.iqgold.data.model.dto.Category;
import io.reactivex.Observable;
import iqgold.ru.iqgold.data.model.dto.Contacts;
import iqgold.ru.iqgold.data.model.dto.Filter;
import iqgold.ru.iqgold.data.model.dto.MessageOrder;
import iqgold.ru.iqgold.data.model.dto.Product;
import iqgold.ru.iqgold.data.model.dto.Sorting;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by Admin on 17.10.2017.
 */

public interface ApiInterface {

    @GET("inc/api/v1/categories/")
    Observable<List<Category>> getCategories(@Query("Id") Long id);

    @GET("inc/api/v1/products/")
    Observable<List<Product>> getProducts(@QueryMap() Map<String, String> options);

    @GET("inc/api/v1/products/{id}")
    Observable<Product> getOneProduct(@Path("id") Long id);

    @GET("inc/api/v1/filters/")
    Observable<List<Filter>> getFilters(@QueryMap() Map<String, String> options);

    @GET("inc/api/v1/sorting/")
    Observable<List<Sorting>> getSorting();

    @GET("inc/api/v1/contacts/")
    Observable<Contacts> getContacts();

    @POST("inc/api/v1/orders")
    Observable<MessageOrder> sendOrder(@QueryMap Map<String, String> order);

}
