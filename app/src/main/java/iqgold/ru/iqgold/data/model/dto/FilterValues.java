package iqgold.ru.iqgold.data.model.dto;

import com.google.gson.annotations.SerializedName;


/**
 * Created by Admin on 28.11.2017.
 */

public class FilterValues  {

    public FilterValues() {
    }

    @SerializedName("title")
    String title;

    @SerializedName("code")
    String code;

    @SerializedName("productsCount")
    long productsCount;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public long getProductsCount() {
        return productsCount;
    }

    public void setProductsCount(long productsCount) {
        this.productsCount = productsCount;
    }
}
