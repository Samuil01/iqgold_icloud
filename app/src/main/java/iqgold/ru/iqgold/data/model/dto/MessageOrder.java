package iqgold.ru.iqgold.data.model.dto;

/**
 * Created by s.kurbanov on 17.12.2017.
 */

public class MessageOrder {

    String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
