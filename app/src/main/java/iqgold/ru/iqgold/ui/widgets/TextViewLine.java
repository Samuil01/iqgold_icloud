package iqgold.ru.iqgold.ui.widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;

/**
 * Created by s.kurbanov on 17.12.2017.
 */

public class TextViewLine extends android.support.v7.widget.AppCompatTextView {

    private Paint paint;

    public TextViewLine(Context context) {
        super(context);
        init();
    }

    public TextViewLine(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextViewLine(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        paint = new Paint();
        paint.setColor(Color.RED);
        //Width
        paint.setStrokeWidth(6);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawLine(0, getHeight() / 2, getWidth(), getHeight() / 2, paint);
    }
}
