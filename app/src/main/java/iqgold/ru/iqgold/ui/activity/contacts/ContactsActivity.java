package iqgold.ru.iqgold.ui.activity.contacts;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import iqgold.ru.iqgold.R;
import iqgold.ru.iqgold.data.model.dto.Contacts;

public class ContactsActivity extends MvpAppCompatActivity implements IContacts {

    @InjectPresenter
    ContactsPresenter presenter;


    private Uri callDial;

    TextView call, callWA, mail, address, schedule;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

        findViewById(R.id.back).setOnClickListener(v -> finish());

        call = (TextView) findViewById(R.id.contacts_phone);
        callWA = (TextView) findViewById(R.id.contacts_wa);
        mail = (TextView) findViewById(R.id.contacts_mail);
        address = (TextView) findViewById(R.id.contacts_address);
        schedule = (TextView) findViewById(R.id.contacts_schedule);

        findViewById(R.id.contacts_call).setOnClickListener(v -> startActivity(new Intent(Intent.ACTION_DIAL, callDial)));

        presenter.getContacts();
    }

    @Override
    public void showContacts(Contacts contacts) {
        callDial = Uri.parse("tel:" + contacts.getPhoneNumber());
        call.setText(contacts.getPhoneNumber());
        callWA.setText(contacts.getSkypeNumber());
        mail.setText(contacts.getEmail());
        address.setText(contacts.getAddress());
        schedule.setText(contacts.getSchedule());
    }

    @Override
    public void showError() {

    }

    @Override
    public void showLoading(boolean b) {
    }
}
