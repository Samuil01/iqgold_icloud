package iqgold.ru.iqgold.ui.activity.product;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.inject.Inject;

import io.reactivex.Observable;
import iqgold.ru.iqgold.App;
import iqgold.ru.iqgold.data.model.dto.Filter;
import iqgold.ru.iqgold.data.model.dto.FilterValues;
import iqgold.ru.iqgold.data.model.dto.Product;
import iqgold.ru.iqgold.data.model.dto.Sorting;
import iqgold.ru.iqgold.data.network.NetModel;
import iqgold.ru.iqgold.other.ProductUtils;

/**
 * Created by Admin on 01.12.2017.
 */

@InjectViewState
public class PresenterProducts extends MvpPresenter<ProductsView> {

    @Inject
    NetModel netModel;

    List<Filter> filterList;

    Map map = new TreeMap<String, Filter>();
    private List<Product> products = new ArrayList<>();
    private Sorting sorting;

    public PresenterProducts() {
        App.getAppComponent().inject(this);
    }

    public void getFilter() {

    }

    public void getData(Long categoryId, FilterParameters filterParameters) {
        Map<String, String> options = getParameters(categoryId, filterParameters);
        Observable<List<Filter>> observable = netModel
                .getFilters(options)
                .map(filters -> {
                    for (Filter filter : filters) {
                        if (filter.getValues() != null) {
                            Map map = new TreeMap<String, FilterValues>();
                            for (FilterValues filterValues : filter.getValues()) {
                                map.put(filterValues.getCode(), filterValues);
                            }
                            filter.setMap(map);
                        }
                    }
                    return filters;
                });

        Observable
                .zip(netModel.getProducts(options), observable
                        , (products, filters) -> {
                            this.products = products;
                            getViewState().setProducts(products);
                            getViewState().setFilters(filters);
                            return "";
                        })
                .doOnSubscribe(disposable -> getViewState().showLoading(true))
                .doAfterTerminate(() -> getViewState().showLoading(false))
                .subscribe(o -> {
                }, throwable -> {
                    String s = "";
                }, () -> {
                    String sd = "";
                });

    }

    private Map<String, String> getParameters(Long categoryId, FilterParameters filterParameters) {
        Map<String, String> options = new HashMap<>();
        options.put("limit", "1000");
        options.put("category", String.valueOf(categoryId));
        options.put("sort", sorting.getCode());
        options.put("order", sorting.getOrder());

        if (filterParameters != null) {
            options = getWithFilters(options, filterParameters);
        }
        return options;
    }

    public Map<String, String> getWithFilters(Map<String, String> options, FilterParameters filterParameters) {
        if (filterParameters.sliders.isEmpty() && filterParameters.checkers.isEmpty()) {
            sendWithoutFilter();
            return options;
        }
        options.putAll(filterParameters.sliders);

        Iterator it = filterParameters.checkers.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            String key = (String) pair.getKey();
            HashSet<String> set = (HashSet<String>) pair.getValue();
            Iterator<String> setIterator = set.iterator();
            while (setIterator.hasNext()) {
                options.put(key, setIterator.next());
            }
        }
        return options;
    }

    public void sendWithoutFilter() {

    }

    public void searchProduct(Observable<String> searchObservable) {
        searchObservable
                .map(s -> ProductUtils.searchProduct(products, s))
                .subscribe(products -> getViewState().setProducts(products));
    }

    public void setSorting(Object sortingObject) {
        sorting = (Sorting) sortingObject;
    }

    public void getSorting() {
        netModel.getSorting()
                .doOnNext(sortingList -> {
                    if (sortingList.isEmpty()) {
                        throw new EmptySortListException("empty sort list");
                    }
                })
                .subscribe(sortingList -> {
                            sorting = sortingList.get(0);
                            getViewState().showSorting(sortingList);
                        },
                        throwable -> getViewState().showError());

    }
}
