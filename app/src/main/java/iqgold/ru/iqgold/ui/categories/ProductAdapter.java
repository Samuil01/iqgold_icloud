package iqgold.ru.iqgold.ui.categories;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import iqgold.ru.iqgold.R;
import iqgold.ru.iqgold.data.model.dto.Product;

/**
 * Created by Admin on 30.11.2017.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    private List<Product> list = new ArrayList<>();

    private OnClickListener onClickListener;

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public void setList(List<Product> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.title.setText(list.get(position).getTitle());
        holder.priceNew.setText(String.valueOf(list.get(position).getPrice()) + " р.");
        if (list.get(position).getOldPrice() == 0) {
            holder.priceOld.setVisibility(View.GONE);
        } else {
            holder.priceOld.setText(String.valueOf(list.get(position).getOldPrice()) + " р.");
        }

        Glide.with(holder.imageView.getContext())
                .load(list.get(position).getMainImage())
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView title;
        TextView priceNew;
        TextView priceOld;

        public ViewHolder(View itemView) {
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.item_product_image);
            title = (TextView) itemView.findViewById(R.id.item_product_title);
            priceNew = (TextView) itemView.findViewById(R.id.item_product_price_new);
            priceOld = (TextView) itemView.findViewById(R.id.item_product_price_old);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.onClick(list.get(getAdapterPosition()).getId());
                }
            });

        }
    }

    public interface OnClickListener {
        void onClick(long id);
    }

}
