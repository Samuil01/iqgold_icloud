package iqgold.ru.iqgold.ui.activity.contacts;

import com.arellomobile.mvp.MvpView;

import iqgold.ru.iqgold.data.model.dto.Contacts;

/**
 * Created by s.kurbanov on 17.12.2017.
 */

public interface IContacts extends MvpView {
    void showContacts(Contacts contacts);

    void showError();

    void showLoading(boolean b);
}
