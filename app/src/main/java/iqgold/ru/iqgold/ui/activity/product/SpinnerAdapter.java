package iqgold.ru.iqgold.ui.activity.product;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import iqgold.ru.iqgold.R;

/**
 * Created by s.kurbanov on 10.12.17.
 */

public class SpinnerAdapter extends ArrayAdapter {

    private int curPosition = 0;

    public void setCurPosition(int curPosition) {
        this.curPosition = curPosition;
        notifyDataSetChanged();
    }

    public SpinnerAdapter(@NonNull Context context, int resource, int textViewResource, int curPosition) {
        super(context, resource, textViewResource);
        this.curPosition = curPosition;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = super.getView(position, convertView, parent);
        ((ImageView) v.findViewById(R.id.item_spinner_checkbox)).setVisibility(View.GONE);
        return v;
    }

    @Nullable
    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = super.getView(position, convertView, parent);
        showSelectItem(v, position == curPosition);
        return v;
    }

    private void showSelectItem(View v, boolean isSelected) {
        int color = isSelected ? R.color.orange : android.R.color.black;
        ((TextView) v.findViewById(R.id.item_spinner_title)).setTextColor(getContext().getResources().getColor(color));
        if (isSelected) {
            ((ImageView) v.findViewById(R.id.item_spinner_checkbox)).setImageResource(R.drawable.checked_orange);
        } else {
            ((ImageView) v.findViewById(R.id.item_spinner_checkbox)).setImageDrawable(null);
        }
    }

    @Override
    public void addAll(Object[] items) {
        clear();
        super.addAll(items);
        notifyDataSetChanged();
    }
}
