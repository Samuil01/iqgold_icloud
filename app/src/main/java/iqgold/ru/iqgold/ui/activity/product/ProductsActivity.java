package iqgold.ru.iqgold.ui.activity.product;

import android.animation.LayoutTransition;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.jakewharton.rxbinding2.widget.RxTextView;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import iqgold.ru.iqgold.R;
import iqgold.ru.iqgold.data.model.dto.Filter;
import iqgold.ru.iqgold.data.model.dto.Product;
import iqgold.ru.iqgold.data.model.dto.Sorting;
import iqgold.ru.iqgold.other.SpacesItemDecoration;
import iqgold.ru.iqgold.other.Utils;
import iqgold.ru.iqgold.ui.activity.oneproduct.OneProductActivity;
import iqgold.ru.iqgold.ui.categories.ProductAdapter;

public class ProductsActivity extends MvpAppCompatActivity
        implements ProductsView, NavigationView.OnNavigationItemSelectedListener {

    public static final String CATEGORY_ID = "CATEGORY_ID";
    private Long categoryId = -1L;
    private ImageView spinnerImage;

    ImageView filter;
    DrawerLayout drawer;

    @InjectPresenter
    PresenterProducts presenter;

    TextView searchCancel;
    EditText search;

    FilterAdapter filterAdapter;
    ProductAdapter productAdapter;
    RecyclerView filterRecyclerView;
    RecyclerView productRecyclerView;
    ProgressBar progressBar;
    Spinner spinner;

    SpinnerAdapter spinnerAdapter;

    private TextView submit;
    private TextView cancel;

    private SpinnerListener spinnerListener = new SpinnerListener();

    private class SpinnerListener implements AdapterView.OnItemSelectedListener, View.OnTouchListener {

        private boolean isUserSelect;

        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            isUserSelect = true;
            return false;
        }

        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            if (isUserSelect) {
                spinnerAdapter.setCurPosition(position);
                presenter.setSorting(spinnerAdapter.getItem(position));
                presenter.getData(categoryId, filterAdapter.getFilterParameters());
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    private ProductAdapter.OnClickListener onItemClickListener = id -> {
        Intent intent = new Intent(ProductsActivity.this, OneProductActivity.class);
        intent.putExtra(OneProductActivity.ONE_PRODUCT_ID_KEY, id);
        startActivity(intent);
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);

        LayoutTransition layoutTransitionInner = new LayoutTransition();
        layoutTransitionInner.setDuration(350);

        layoutTransitionInner.enableTransitionType(LayoutTransition.CHANGING);
        ((LinearLayout) findViewById(R.id.search_linear))
                .setLayoutTransition(layoutTransitionInner);

        searchCancel = (TextView) findViewById(R.id.search_cancel);
        searchCancel.setOnClickListener(v -> {
            v.setVisibility(View.GONE);
            Utils.closeSoftKeyboard(this);
            search.setText("");
            search.clearFocus();
        });

        search = (EditText) findViewById(R.id.edit_text_search);
        search.setOnTouchListener((v, event) -> {
            searchCancel.setVisibility(View.VISIBLE);
            Utils.showSoftKeyboard(this, search);
            return false;
        });

        findViewById(R.id.back).setOnClickListener(v -> finish());

        spinner = (Spinner) findViewById(R.id.spinner);
        spinnerAdapter = new SpinnerAdapter(this, R.layout.item_spinner, R.id.item_spinner_title, 0);
        spinner.setAdapter(spinnerAdapter);
        spinner.setOnItemSelectedListener(spinnerListener);
        spinner.setOnTouchListener(spinnerListener);

        spinnerImage = (ImageView) findViewById(R.id.spinner_sort_icon);
        /*spinnerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinner.onTouchEvent(new MotionEvent());
            }
        });*/

        setUpSearch();

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        submit = (TextView) findViewById(R.id.filter_submit);
        cancel = (TextView) findViewById(R.id.filter_cancel);

        submit.setOnClickListener(v -> {
            closeOpenDrawer();
            presenter.getData(categoryId, filterAdapter.getFilterParameters());
        });

        cancel.setOnClickListener(v -> {
            closeOpenDrawer();
            presenter.getData(categoryId, null);
        });

        filterRecyclerView = (RecyclerView) findViewById(R.id.filter_recycler_view);
        filterAdapter = new FilterAdapter(this);
        filterRecyclerView.setAdapter(filterAdapter);

        productRecyclerView = (RecyclerView) findViewById(R.id.product_recycle_view);
        productRecyclerView.setOnTouchListener((v, event) -> {
            Utils.closeSoftKeyboard(this);
            return false;
        });
        productRecyclerView.addItemDecoration(new SpacesItemDecoration(30));
        productRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        productAdapter = new ProductAdapter();
        productAdapter.setOnClickListener(onItemClickListener);
        productRecyclerView.setAdapter(productAdapter);

        categoryId = getIntent().getLongExtra(CATEGORY_ID, -1L);
        presenter.getSorting();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        filter = (ImageView) findViewById(R.id.filter);
        filter.setOnClickListener(v -> closeOpenDrawer());

        setUpSearch();
    }

    private void setUpSearch() {
        Observable<String> searchObservable = RxTextView.textChanges(search)
                .debounce(600, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .map(String::valueOf);

        presenter.searchProduct(searchObservable);
    }

    private void closeOpenDrawer() {
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            drawer.openDrawer(GravityCompat.END);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.products, menu);
        return true;
    }*/

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void setProducts(List<Product> products) {
        productAdapter.setList(products);
    }

    @Override
    public void setFilters(List<Filter> filters) {
        filterAdapter.setFilters(filters);
    }

    @Override
    public void showLoading(boolean isLoading) {
        if (isLoading) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void showSorting(List<Sorting> sortingList) {
        spinnerImage.setVisibility(View.VISIBLE);
        spinnerAdapter.addAll(sortingList);
        spinnerAdapter.setCurPosition(0);

        presenter.getData(categoryId, null);
    }

    @Override
    public void showError() {
        Snackbar snackbar = Snackbar.make(drawer, getResources().getString(R.string.error_message), Snackbar.LENGTH_INDEFINITE)
                .setAction("Повторить", view -> presenter.getData(categoryId, null));
        snackbar.show();
        /*Toast.makeText(this, getResources().getString(R.string.error_message), Toast.LENGTH_LONG).show();*/
    }
}
