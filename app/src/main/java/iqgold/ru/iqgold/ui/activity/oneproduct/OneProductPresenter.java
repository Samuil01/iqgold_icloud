package iqgold.ru.iqgold.ui.activity.oneproduct;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import javax.inject.Inject;

import io.reactivex.functions.Consumer;
import iqgold.ru.iqgold.App;
import iqgold.ru.iqgold.data.model.dto.Product;
import iqgold.ru.iqgold.data.network.NetModel;

/**
 * Created by s.kurbanov on 11.12.17.
 */

@InjectViewState
public class OneProductPresenter extends MvpPresenter<OneProductView> {

    @Inject
    NetModel netModel;

    public OneProductPresenter() {
        App.getAppComponent().inject(this);
    }

    public void getOneProduct(Long id) {
        if (id == -1) {
            getViewState().showError();
            return;
        }

        netModel
                .getOneProduct(id)
                .doOnSubscribe((disposable) -> getViewState().showLoading(true))
                .doAfterTerminate(() -> getViewState().showLoading(false))
                .subscribe(new Consumer<Product>() {
                    @Override
                    public void accept(Product product) throws Exception {
                        getViewState().showProduct(product);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        getViewState().showError();
                    }
                });

    }
}
