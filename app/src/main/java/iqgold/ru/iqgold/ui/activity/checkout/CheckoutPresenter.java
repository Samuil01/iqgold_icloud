package iqgold.ru.iqgold.ui.activity.checkout;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import iqgold.ru.iqgold.App;

import iqgold.ru.iqgold.data.model.dto.Order;
import iqgold.ru.iqgold.data.network.NetModel;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;

/**
 * Created by s.kurbanov on 17.12.2017.
 */

@InjectViewState
public class CheckoutPresenter extends MvpPresenter<ICheckout> {

    @Inject
    NetModel netModel;

    public static final MediaType MEDIA_TYPE =
            MediaType.parse("application/json");

    public CheckoutPresenter() {
        App.getAppComponent().inject(this);
    }

    final OkHttpClient client = new OkHttpClient();

    public void sendOrder(Order order) {
        Map<String, String> params = createQueryParams(order);

        netModel.sendOrder(params)
                .doOnSubscribe(disposable -> getViewState().showProgress())
                .subscribe(messageOrder -> getViewState().showMessage(messageOrder.getMessage())
                        , throwable -> getViewState().showError());

    }

    private Map<String, String> createQueryParams(Order order) {
        Map<String, String> params = new HashMap<>(4);
        params.put("product", String.valueOf(order.getProduct()));
        params.put("name", String.valueOf(order.getName()));
        params.put("email", String.valueOf(order.getEmail()));
        params.put("phoneNumber", String.valueOf(order.getPhoneNumber()));
        return params;
    }

}
