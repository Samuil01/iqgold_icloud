package iqgold.ru.iqgold.ui.activity.oneproduct;

import com.arellomobile.mvp.MvpView;

import iqgold.ru.iqgold.data.model.dto.Product;

/**
 * Created by s.kurbanov on 11.12.17.
 */

interface OneProductView extends MvpView{

    void showLoading(boolean b);

    void showError();

    void showProduct(Product product);

}
