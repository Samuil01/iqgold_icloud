package iqgold.ru.iqgold.ui.activity.checkout;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import iqgold.ru.iqgold.R;
import iqgold.ru.iqgold.data.model.dto.Order;
import iqgold.ru.iqgold.other.Utils;
import okhttp3.internal.Util;

import static iqgold.ru.iqgold.ui.activity.oneproduct.OneProductActivity.ONE_PRODUCT_ID_KEY;

public class CheckoutActivity extends MvpAppCompatActivity implements ICheckout {

    @InjectPresenter
    CheckoutPresenter presenter;
    EditText name, email, phoneNumber;
    ProgressBar progressBar;

    private MaterialDialog dialog;

    Long id = 0L;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        findViewById(R.id.back).setOnClickListener(v -> finish());

        id = getIntent().getLongExtra(ONE_PRODUCT_ID_KEY, -1);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        findViewById(R.id.send).setOnClickListener(v -> {
            if (checkForEmpty()) {
                Utils.closeSoftKeyboard(CheckoutActivity.this);
                sendOrder();
            } else {
                Toast.makeText(this, "Не все поля заполнены", Toast.LENGTH_LONG).show();
            }
        });

        name = (EditText) findViewById(R.id.name);
        email = (EditText) findViewById(R.id.email);
        phoneNumber = (EditText) findViewById(R.id.phone_number);

    }

    private boolean checkForEmpty() {
        return !TextUtils.isEmpty(name.getText()) && !TextUtils.isEmpty(email.getText()) && !TextUtils.isEmpty(phoneNumber.getText());
    }

    @Override
    public void showLoading(boolean isLoading) {
        if (isLoading) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void sendOrder() {
        Order order = new Order();
        order.setName(name.getText().toString());
        order.setEmail(email.getText().toString());
        order.setPhoneNumber(phoneNumber.getText().toString());
        order.setProduct(id);

        presenter.sendOrder(order);
    }

    @Override
    public void showError() {
        dialog.dismiss();
        Toast.makeText(this, getResources().getString(R.string.error_message), Toast.LENGTH_LONG).show();
    }

    @Override
    public void showMessage(String message) {
        resultOK(message);
    }

    @Override
    public void showProgress() {
        showLoading();
    }

    public void showLoading() {
        dialog = new MaterialDialog.Builder(this)
                .title(R.string.please_wait)
                //.content(R.string.please_wait)
                .progress(true, 0)
                .cancelable(false)
                .progressIndeterminateStyle(true)
                .build();
        dialog.show();
    }

    public void resultOK(String message) {
        dialog.getProgressBar().setVisibility(View.GONE);
        dialog.setContent(message);
        dialog.setTitle(R.string.order_confirmed);
        dialog.setActionButton(DialogAction.NEGATIVE, R.string.close_message);
        dialog.getActionButton(DialogAction.NEGATIVE).setOnClickListener(v -> {
            dialog.dismiss();
            finish();
        });
    }

}
