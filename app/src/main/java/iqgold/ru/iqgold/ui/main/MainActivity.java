package iqgold.ru.iqgold.ui.main;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.List;

import javax.inject.Inject;

import iqgold.ru.iqgold.App;
import iqgold.ru.iqgold.R;
import iqgold.ru.iqgold.other.Screens;
import iqgold.ru.iqgold.ui.categories.CategoriesFragment;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;
import ru.terrakok.cicerone.android.SupportFragmentNavigator;
import ru.terrakok.cicerone.commands.Command;

public class MainActivity extends MvpAppCompatActivity implements MainView {

    @InjectPresenter
    MainActivityPresenter presenter;

    @Inject
    NavigatorHolder navigatorHolder;

    @Inject
    Router router;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        App.getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        router.replaceScreen(Screens.CATEGORY_FRAGMENT);
    }

    private Navigator navigator = new SupportFragmentNavigator(getSupportFragmentManager(), R.id.root) {
        @Override
        protected Fragment createFragment(String screenKey, Object data) {
            switch (screenKey) {
                case Screens.CATEGORY_FRAGMENT:
                    return CategoriesFragment.newInstance();
                default:
                    return CategoriesFragment.newInstance();
            }
        }

        @Override
        protected void showSystemMessage(String message) {
            Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void exit() {
            finish();
        }

        @Override
        public void applyCommand(Command command) {
            super.applyCommand(command);
        }
    };

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        super.onPause();
        navigatorHolder.removeNavigator();
    }

    @Override
    public void onBackPressed() {

        List<Fragment> fragmentsList = getSupportFragmentManager().getFragments();
        for (Fragment fragment: fragmentsList) {
            if (fragment instanceof CategoriesFragment) {
                ((CategoriesFragment) fragment).onBackPressed();
            }
        }

    }
}
