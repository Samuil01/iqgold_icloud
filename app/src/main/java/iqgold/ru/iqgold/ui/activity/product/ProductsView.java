package iqgold.ru.iqgold.ui.activity.product;

import com.arellomobile.mvp.MvpView;

import java.util.List;

import iqgold.ru.iqgold.data.model.dto.Filter;
import iqgold.ru.iqgold.data.model.dto.Product;
import iqgold.ru.iqgold.data.model.dto.Sorting;

public interface ProductsView extends MvpView {

    void setProducts(List<Product> products);

    void setFilters(List<Filter> filters);

    void showLoading(boolean isLoading);

    void showSorting(List<Sorting> sortingList);

    void showError();
}
