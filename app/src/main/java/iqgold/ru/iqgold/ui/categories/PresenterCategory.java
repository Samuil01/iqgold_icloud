package iqgold.ru.iqgold.ui.categories;

import com.arellomobile.mvp.InjectViewState;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Predicate;
import iqgold.ru.iqgold.App;
import iqgold.ru.iqgold.data.model.Preferences;
import iqgold.ru.iqgold.data.model.dto.Category;
import iqgold.ru.iqgold.data.model.dto.Contacts;
import iqgold.ru.iqgold.data.network.NetModel;
import ru.terrakok.cicerone.Router;

/**
 * Created by Admin on 30.11.2017.
 */

@InjectViewState
public class PresenterCategory extends com.arellomobile.mvp.MvpPresenter<CategoryView> {

    private List<Category> categoryList = new ArrayList<>();
    private boolean isFirst = true;

    public PresenterCategory() {
        App.getAppComponent().inject(this);
    }

    private Queue<Long> queue = new LinkedList<>();

    @Inject
    NetModel netModel;

    @Inject
    Router router;

    @Inject
    Preferences preferences;

    Long prevId = -1L;

    public void getCategories(Long id) {
        netModel.getCategories(id)
                .compose(applyLoader())
                .retry(2)
                .subscribe(categories -> {
                            setIdToQueue(id);
                            categoryList = categories;
                            getViewState().showCategories(categories);
                        }
                        , throwable -> getViewState().showError());
    }

    private void setIdToQueue(Long id) {
        if (prevId != id) {
            queue.add(id);
            prevId = id;
        }
    }

    public void onBackPressed() {
        Long id = queue.poll();
        if (id == null) {
            router.exit();
        } else {
            getCategories(id);
        }
    }

    public void searchProduct(Observable<String> searchObservable) {
        searchObservable
                .compose(applyLoader())
                .subscribe(s -> {
                    int length = s.length();
                    if (length > 2) {
                        if (!isFirst) {
                            getProducts(s);
                        }
                        isFirst = false;
                    } else if (length > 0 && length <= 2) {
                        isFirst = false;
                        // do nothing
                    } else if (length == 0) {
                        if (!isFirst) {
                            getViewState().showCategories(categoryList);
                        }
                        isFirst = false;
                    }
                });
    }

    private void getProducts(String s) {
        Map<String, String> options = getParameters(s);
        netModel.getProducts(options)
                .compose(applyLoader())
                .subscribe(products -> getViewState().showProducts(products),
                        throwable -> getViewState().showError()
                );
    }

    private <T> ObservableTransformer<T, T> applyLoader() {
        return tObservable -> tObservable
                .doOnSubscribe((disposable) -> getViewState().showLoading(true))
                .doAfterTerminate(() -> getViewState().showLoading(false));

    }

    private Map<String, String> getParameters(String s) {
        Map<String, String> options = new HashMap<>();
        options.put("limit", "1000");
        options.put("titleContains", s);
        return options;
    }

    public void getContacts() {
        netModel.getContacts()
                .doOnSubscribe(s -> getViewState().showLoading(true))
                .doOnTerminate(() -> getViewState().showLoading(false))
                .doOnNext(contacts -> preferences.setUser(contacts))
                .subscribe(contacts -> {
                }, throwable -> /*getViewState().showError()*/ {
                });
    }

}
