package iqgold.ru.iqgold.ui.activity.product;

/**
 * Created by s.kurbanov on 10.12.17.
 */

public class EmptySortListException extends Exception {
    public EmptySortListException(String message) {
        super(message);
    }
}
