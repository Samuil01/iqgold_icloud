package iqgold.ru.iqgold.ui.activity.checkout;

import com.arellomobile.mvp.MvpView;

/**
 * Created by s.kurbanov on 17.12.2017.
 */

public interface ICheckout extends MvpView {

    void showLoading(boolean isLoading);

    void sendOrder();

    void showError();

    void showMessage(String message);

    void showProgress();
}
