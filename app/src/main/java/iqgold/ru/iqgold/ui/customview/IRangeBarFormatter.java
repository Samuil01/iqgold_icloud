package iqgold.ru.iqgold.ui.customview;

/**
 * Created by Admin on 02.12.2017.
 */

public interface IRangeBarFormatter {
    String format(String value);
}
