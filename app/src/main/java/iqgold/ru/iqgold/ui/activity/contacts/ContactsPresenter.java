package iqgold.ru.iqgold.ui.activity.contacts;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import javax.inject.Inject;

import iqgold.ru.iqgold.App;
import iqgold.ru.iqgold.data.model.Preferences;
import iqgold.ru.iqgold.data.model.dto.Contacts;
import iqgold.ru.iqgold.data.network.NetModel;

/**
 * Created by s.kurbanov on 17.12.2017.
 */

@InjectViewState
public class ContactsPresenter extends MvpPresenter<IContacts> {

    @Inject
    NetModel netModel;

    @Inject
    Preferences preferences;

    public ContactsPresenter() {
        App.getAppComponent().inject(this);
    }

    public void getContacts() {
        Contacts cache = preferences.getContacts();
        if (cache != null) {
            getViewState().showContacts(cache);
        }
        netModel.getContacts()
                /*.startWith(preferences.getContacts())*/
                .doOnSubscribe(s -> getViewState().showLoading(true))
                .doOnTerminate(() -> getViewState().showLoading(false))
                .doOnNext(contacts -> preferences.setUser(contacts))
                .subscribe(contacts -> getViewState().showContacts(contacts), throwable -> getViewState().showError());
    }
}
