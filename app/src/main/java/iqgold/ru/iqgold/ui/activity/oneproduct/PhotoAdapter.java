package iqgold.ru.iqgold.ui.activity.oneproduct;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

import iqgold.ru.iqgold.R;

/**
 * Created by s.kurbanov on 11.12.17.
 */

class PhotoAdapter extends PagerAdapter {

    private List<String> gallery;
    private Context context;
    private String mainImage = "";

    public PhotoAdapter(List<String> gallery, String mainImage, Context context) {
        this.gallery = gallery;
        this.mainImage = mainImage;
        this.context = context;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = new ImageView(context);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        Glide.with(imageView.getContext())
                .load(mainImage.isEmpty() ? mainImage : gallery.get(position))
                    /*.diskCacheStrategy(DiskCacheStrategy.ALL)*/
                    /*.placeholder(drawable)*/
                    /*.error(drawable)*/
                //.placeholder(R.placeHolder.place_foto_stub)
                //.centerCrop()
                .into(imageView);
        container.addView(imageView);
        return imageView;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override

    public int getCount() {
        if (gallery.isEmpty()) {
            return 1;
        } else {
            return gallery.size();
        }
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }
}
