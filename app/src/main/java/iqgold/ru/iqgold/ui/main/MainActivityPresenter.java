package iqgold.ru.iqgold.ui.main;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import javax.inject.Inject;
import ru.terrakok.cicerone.Router;

/**
 * Created by Admin on 30.11.2017.
 */

@InjectViewState
public class MainActivityPresenter extends MvpPresenter<MainView>{

}
