package iqgold.ru.iqgold.ui.categories;

import com.arellomobile.mvp.MvpView;

import java.util.List;

import iqgold.ru.iqgold.data.model.dto.Category;
import iqgold.ru.iqgold.data.model.dto.Contacts;
import iqgold.ru.iqgold.data.model.dto.Product;

/**
 * Created by Admin on 30.11.2017.
 */

public interface CategoryView extends MvpView {

    void showCategories(List<Category> categories);

    void showError();

    void showLoading(boolean isLoading);

    void showProducts(List<Product> products);
}

