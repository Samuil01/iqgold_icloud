package iqgold.ru.iqgold.ui.activity.product;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import iqgold.ru.iqgold.R;
import iqgold.ru.iqgold.data.model.dto.Filter;
import iqgold.ru.iqgold.data.model.dto.FilterValues;
import iqgold.ru.iqgold.ui.customview.RangeBar;

/**
 * Created by Admin on 02.12.2017.
 */

public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.ViewHolder> {

    private List<Filter> list = new ArrayList<>(5);

    private static final int VIEW_TYPE_SLIDER = 0;
    private static final int VIEW_TYPE_CHECK = 1;

    public void setList(List<Filter> list) {
        this.list = list;
    }

    private Context context;

    Drawable plus, minus;
    int titleGray, titleGreen;

    private FilterParameters filterParameters = new FilterParameters();

    public FilterParameters getFilterParameters() {
        return filterParameters;
    }

    public void setFilterParameters(FilterParameters filterParameters) {
        this.filterParameters = filterParameters;
    }

    public FilterAdapter(Context context) {
        this.context = context;
        plus = ContextCompat.getDrawable(context, R.drawable.plus);
        minus = ContextCompat.getDrawable(context, R.drawable.minus);
        titleGray = ContextCompat.getColor(context, R.color.filter_title_gray);
        titleGreen = ContextCompat.getColor(context, R.color.green_lite);
    }

    @Override
    public FilterAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;

        if (viewType == VIEW_TYPE_SLIDER) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_range, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_check, parent, false);
        }

        return new ViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(FilterAdapter.ViewHolder holder, int position) {
        holder.title.setText(list.get(position).getTitle());
        if (holder.getItemViewType() == VIEW_TYPE_SLIDER) {
            if ("Цена".equals(list.get(position).getTitle())) {
                holder.rangeBar.setTickInterval(10);
            }
            if (list.get(position).getMaxValue() == list.get(position).getMinValue()) {
                holder.rangeBar.setEnabled(false);
            } else {
                holder.rangeBar.setEnabled(true);
                holder.rangeBar.setTickEnd((float) list.get(position).getMaxValue());
                holder.rangeBar.setTickStart((float) list.get(position).getMinValue());
            }
            holder.rangeStart.setText("" + (int) list.get(position).getMinValue());
            holder.rangeEnd.setText("" + (int) list.get(position).getMaxValue());
            holder.rangeBar.setOnRangeBarChangeListener((rangeBar, leftPinIndex, rightPinIndex, leftPinValue, rightPinValue) -> {
                filterParameters.sliders.put(list.get(position).getMinParameter(), leftPinValue);
                filterParameters.sliders.put(list.get(position).getMaxParameter(), rightPinValue);
                holder.rangeStart.setText(leftPinValue);
                holder.rangeEnd.setText(rightPinValue);
            });
        } else {
            holder.innerLayout.removeAllViews();
            HashSet<String> filters = filterParameters.checkers.get(list.get(position).getParameter());
            for (FilterValues filterValues : list.get(position).getValues()) {
                View view = LayoutInflater.from(context).inflate(R.layout.item_check_inner, null, false);
                TextView textView = view.findViewById(R.id.item_filter_inner_title);
                ImageView imageView = view.findViewById(R.id.item_filter_inner_image);

                if (filters != null) {
                    if (filters.contains(filterValues.getCode())) {
                        imageView.setImageResource(R.drawable.checked);
                    } else {
                        imageView.setImageDrawable(null);
                    }
                }
                view.setOnClickListener(v -> {
                    HashSet<String> filtersInner = filterParameters.checkers.get(list.get(position).getParameter());
                    if (filtersInner == null) {
                        filtersInner = new HashSet<String>();
                    }
                    if (imageView.getDrawable() == null) {
                        filtersInner.add(filterValues.getCode());
                        imageView.setImageResource(R.drawable.checked);
                    } else {
                        filtersInner.remove(filterValues.getCode());
                        imageView.setImageDrawable(null);
                    }
                    if (filtersInner.isEmpty()) {
                        filterParameters.checkers.remove(list.get(position).getParameter());
                    } else {
                        filterParameters.checkers.put(list.get(position).getParameter(), filtersInner);
                    }
                });
                textView.setText(filterValues.getTitle());
                holder.innerLayout.addView(view);
            }
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setFilters(List<Filter> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if ("slider".equals(list.get(position).getType())) {
            return VIEW_TYPE_SLIDER;
        } else {
            return VIEW_TYPE_CHECK;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        ExpandableLayout expandableLayout;
        ImageView plusMinus;

        RangeBar rangeBar;
        TextView rangeStart;
        TextView rangeEnd;

        LinearLayout innerLayout;

        public ViewHolder(View itemView, int viewType) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.item_filter_title);
            expandableLayout = (ExpandableLayout) itemView.findViewById(R.id.expandable_layout_0);
            plusMinus = (ImageView) itemView.findViewById(R.id.item_filter_plus);
            title.setOnClickListener(v -> {
                if (expandableLayout.isExpanded()) {
                    title.setTextColor(titleGray);
                    plusMinus.setImageDrawable(plus);
                    expandableLayout.collapse();
                } else {
                    plusMinus.setImageDrawable(minus);
                    title.setTextColor(titleGreen);
                    expandableLayout.expand();
                }
            });
            if (viewType == VIEW_TYPE_SLIDER) {
                rangeBar = (RangeBar) itemView.findViewById(R.id.range_bar);
                rangeStart = (TextView) itemView.findViewById(R.id.item_filter_range_start);
                rangeEnd = (TextView) itemView.findViewById(R.id.item_filter_range_end);
            } else {
                innerLayout = (LinearLayout) itemView.findViewById(R.id.item_filter_layout_inner);
            }
        }
    }
}
