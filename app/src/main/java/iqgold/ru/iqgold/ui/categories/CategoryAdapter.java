package iqgold.ru.iqgold.ui.categories;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import iqgold.ru.iqgold.R;
import iqgold.ru.iqgold.data.model.dto.Category;

/**
 * Created by Admin on 30.11.2017.
 */

class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    private List<Category> list = new ArrayList<>();

    public void setList(List<Category> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    private onClickListener onClickListener;

    public void setOnClickListener(CategoryAdapter.onClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.title.setText(list.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView title;

        public ViewHolder(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.item_category_title);

            title.setOnClickListener(v -> {
                int position = getAdapterPosition();
                onClickListener.onClick(list.get(position).getId(),
                        list.get(position).getContentType());
            });

        }
    }

    interface onClickListener {
        void onClick(long id, String contentType);
    }
}
