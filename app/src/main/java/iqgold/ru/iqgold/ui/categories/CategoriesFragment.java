package iqgold.ru.iqgold.ui.categories;

import android.animation.LayoutTransition;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.jakewharton.rxbinding2.widget.RxTextView;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import iqgold.ru.iqgold.R;
import iqgold.ru.iqgold.data.model.dto.Category;
import iqgold.ru.iqgold.data.model.dto.Contacts;
import iqgold.ru.iqgold.data.model.dto.Product;
import iqgold.ru.iqgold.other.SpacesItemDecoration;
import iqgold.ru.iqgold.other.SpacesItemDecorationVertical;
import iqgold.ru.iqgold.other.Utils;
import iqgold.ru.iqgold.ui.activity.contacts.ContactsActivity;
import iqgold.ru.iqgold.ui.activity.oneproduct.OneProductActivity;
import iqgold.ru.iqgold.ui.activity.product.ProductsActivity;
import okhttp3.internal.Util;

public class CategoriesFragment extends MvpAppCompatFragment implements CategoryView {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    @InjectPresenter
    PresenterCategory presenter;

    private TextView searchCancel;
    private EditText search;
    private RecyclerView recyclerView;
    private CategoryAdapter categoryAdapter = new CategoryAdapter();
    private ProductAdapter productAdapter = new ProductAdapter();
    private ProgressBar progressBar;
    private SpacesItemDecorationVertical spacesItemDecorationVertical = new SpacesItemDecorationVertical(6);
    private SpacesItemDecoration spacesItemDecoration = new SpacesItemDecoration(10);

    private CategoryAdapter.onClickListener categoryClickListener = new CategoryAdapter.onClickListener() {
        @Override
        public void onClick(long id, String contentType) {
            if ("categories".equals(contentType)) {
                presenter.getCategories(id);
            } else if ("goods".equals(contentType)) {
                Intent intent = new Intent(getActivity(), ProductsActivity.class);
                intent.putExtra(ProductsActivity.CATEGORY_ID, id);
                startActivity(intent);
                // presenter.getProduct;
            }
        }
    };

    private ProductAdapter.OnClickListener productOnClickListener = new ProductAdapter.OnClickListener() {
        @Override
        public void onClick(long id) {
            Intent intent = new Intent(getActivity(), OneProductActivity.class);
            intent.putExtra(OneProductActivity.ONE_PRODUCT_ID_KEY, id);
            startActivity(intent);
        }
    };

    public CategoriesFragment() {
        // Required empty public constructor
    }

    private void setUpSearch() {
        Observable<String> searchObservable = RxTextView.textChanges(search)
                .debounce(600, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .map(String::valueOf);

        presenter.searchProduct(searchObservable);
    }

    public static CategoriesFragment newInstance() {
        CategoriesFragment fragment = new CategoriesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_categories, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LayoutTransition layoutTransitionInner = new LayoutTransition();
        layoutTransitionInner.setDuration(350);
        //layoutTransition.setStartDelay(LayoutTransition.CHANGING, 1000);
        layoutTransitionInner.enableTransitionType(LayoutTransition.CHANGING);
        ((LinearLayout) getView().findViewById(R.id.search_linear))
                .setLayoutTransition(layoutTransitionInner);

        searchCancel = (TextView) view.findViewById(R.id.search_cancel);
        searchCancel.setOnClickListener(v -> {
            v.setVisibility(View.GONE);
            Utils.closeSoftKeyboard(getActivity());
            search.setText("");
            search.clearFocus();
        });

        view.findViewById(R.id.call).setOnClickListener(v -> {
            startActivity(new Intent(getActivity(), ContactsActivity.class));
        });

        recyclerView = (RecyclerView) view.findViewById(R.id.recycle_view);

        recyclerView.setOnTouchListener((v, event) -> {
            Utils.closeSoftKeyboard(getActivity());
            return false;
        });

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        presenter.getCategories(null);

        search = (EditText) view.findViewById(R.id.edit_text_search);

        search.setOnTouchListener((v, event) -> {
            searchCancel.setVisibility(View.VISIBLE);
            Utils.showSoftKeyboard(getContext(), search);
            return false;
        });

        setUpSearch();
        presenter.getContacts();
    }

    @Override
    public void showCategories(List<Category> categories) {
        if (recyclerView.getAdapter() instanceof CategoryAdapter) {
            return;
        }
        /*categoryAdapter = new CategoryAdapter();*/
        initCategoryAdapter();
        recyclerView.setAdapter(categoryAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.removeItemDecoration(spacesItemDecoration);
        recyclerView.removeItemDecoration(spacesItemDecorationVertical);
        recyclerView.addItemDecoration(spacesItemDecorationVertical);
        categoryAdapter.setList(categories);
        /*categoryAdapter.notifyDataSetChanged();*/
        /*Glide.with(this).onDestroy();*/

    }

    private void initCategoryAdapter() {
        productAdapter = null;
        categoryAdapter = new CategoryAdapter();
        categoryAdapter.setOnClickListener(categoryClickListener);

    }

    private void initProductAdapter() {
        categoryAdapter = null;
        productAdapter = new ProductAdapter();
        productAdapter.setOnClickListener(productOnClickListener);

    }

    @Override
    public void showError() {
        Snackbar snackbar = Snackbar.make(getView(), getResources().getString(R.string.error_message), Snackbar.LENGTH_INDEFINITE)
                .setAction("Повторить", view -> presenter.getCategories(null));
        snackbar.show();
        /*Toast.makeText(getActivity(), getResources().getString(R.string.error_message), Toast.LENGTH_LONG).show();*/
    }

    @Override
    public void showLoading(boolean isLoading) {
        if (isLoading) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void showProducts(List<Product> products) {
        initProductAdapter();
        recyclerView.setAdapter(productAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        recyclerView.removeItemDecoration(spacesItemDecorationVertical);
        recyclerView.removeItemDecoration(spacesItemDecoration);
        recyclerView.addItemDecoration(spacesItemDecoration);
        productAdapter.setList(products);
    }

    public void onBackPressed() {
        presenter.onBackPressed();
    }

}
