package iqgold.ru.iqgold.ui.activity.oneproduct;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.List;

import iqgold.ru.iqgold.R;
import iqgold.ru.iqgold.data.model.dto.Attribute;
import iqgold.ru.iqgold.data.model.dto.Product;
import iqgold.ru.iqgold.ui.activity.checkout.CheckoutActivity;
import iqgold.ru.iqgold.ui.activity.product.ProductsActivity;
import me.relex.circleindicator.CircleIndicator;

public class OneProductActivity extends MvpAppCompatActivity implements OneProductView {

    public static final String ONE_PRODUCT_ID_KEY = "ONE_PRODUCT_ID_KEY";

    ViewPager viewPager;
    CircleIndicator circleIndicator;
    PhotoAdapter adapter;
    ProgressBar progressBar;
    LinearLayout attrLayoutRoot;

    TextView character, desc, price, oldPrice, title;
    Drawable leftPress, leftEnable, rightPress, rightEnabled;
    int greenColor, whiteColor;

    LinearLayout switchLayout;
    TextView attrMain1, attrMain2;

    Long id = 0L;

    @InjectPresenter
    OneProductPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_product);

        initDrawablesAndColor();

        findViewById(R.id.back).setOnClickListener(v -> finish());

        title = (TextView) findViewById(R.id.one_product_title);

        price = (TextView) findViewById(R.id.one_product_price);
        oldPrice = (TextView) findViewById(R.id.one_product_old_price);

        attrMain1 = (TextView) findViewById(R.id.main_attr_2);
        attrMain2 = (TextView) findViewById(R.id.main_attr_1);

        switchLayout = (LinearLayout) findViewById(R.id.switch_layout);

        attrLayoutRoot = (LinearLayout) findViewById(R.id.product_attribute_layout);

        character = (TextView) findViewById(R.id.product_character);
        desc = (TextView) findViewById(R.id.product_desc);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        id = getIntent().getLongExtra(ONE_PRODUCT_ID_KEY, -1);

        viewPager = (ViewPager) findViewById(R.id.image_viewpager);
        circleIndicator = (CircleIndicator) findViewById(R.id.indicator);

        findViewById(R.id.send).setOnClickListener(v -> {
            Intent intent = new Intent(OneProductActivity.this, CheckoutActivity.class);
            intent.putExtra(OneProductActivity.ONE_PRODUCT_ID_KEY, id);
            startActivity(intent);
        });

        presenter.getOneProduct(id);

    }

    private void initDrawablesAndColor() {
        greenColor = ContextCompat.getColor(this, R.color.green_lite);
        whiteColor = ContextCompat.getColor(this, android.R.color.white);
        leftEnable = getResources().getDrawable(R.drawable.product_desc_enabled_left);
        leftPress = getResources().getDrawable(R.drawable.product_desc_press_left);
        rightEnabled = getResources().getDrawable(R.drawable.product_desc_enabled_right);
        rightPress = getResources().getDrawable(R.drawable.product_desc_press_right);
    }

    @Override
    public void showLoading(boolean isLoading) {
        if (isLoading) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void showError() {
        Toast.makeText(this, getResources().getString(R.string.error_message), Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProduct(Product product) {

        title.setText(product.getTitle());

        price.setText(product.getPrice() + " р.");
        if (product.getOldPrice() != 0) {
            oldPrice.setText(product.getOldPrice() + " р.");
        } else {
            oldPrice.setVisibility(View.GONE);
        }

        setMainAttributes(product.getMainAttributes());

        if (TextUtils.isEmpty(product.getDescription())) {
            switchLayout.setVisibility(View.GONE);
        } else {
            switchLayout.setVisibility(View.VISIBLE);
            character.setOnClickListener(v -> onCharactersChange(false));
            desc.setOnClickListener(v -> onCharactersChange(true));
        }

        if (product.getGallery().size() >= 2) {
            circleIndicator.setVisibility(View.VISIBLE);
        }

        adapter = new PhotoAdapter(product.getGallery(), product.getMainImage(), this);
        viewPager.setAdapter(adapter);
        circleIndicator.setViewPager(viewPager);
        adapter.registerDataSetObserver(circleIndicator.getDataSetObserver());

        setAttributes(product.getAttributes(), product.getMainAttributes());

    }

    private void setMainAttributes(List<Attribute> attributes) {
        Attribute attribute1 = attributes.get(0);
        Attribute attribute2 = attributes.get(1);
        attrMain1.setText(attribute1.getKey() + ": " + attribute1.getValue());
        attrMain2.setText(attribute2.getKey() + ": " + attribute2.getValue());
    }

    private void setAttributes(List<Attribute> attributes, List<Attribute> mainAttributes) {

        for (Attribute attribute : attributes) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            LinearLayout layoutAttribute = (LinearLayout) inflater.inflate(R.layout.item_attribute, null);
            layoutAttribute.findViewById(R.id.product_attribute_layout);
            TextView key = (TextView) layoutAttribute.findViewById(R.id.item_attribute_key);
            key.setText(attribute.getKey() + ": ");
            TextView value = (TextView) layoutAttribute.findViewById(R.id.item_attribute_value);
            value.setText(attribute.getValue());
            attrLayoutRoot.addView(layoutAttribute);
        }

    }

    public void onCharactersChange(boolean isDesk) {

        if (isDesk) {
            character.setBackground(leftEnable);
            desc.setBackground(rightPress);

            character.setTextColor(greenColor);
            desc.setTextColor(whiteColor);

        } else {
            character.setBackground(leftPress);
            desc.setBackground(rightEnabled);

            character.setTextColor(whiteColor);
            desc.setTextColor(greenColor);

        }
    }
}
